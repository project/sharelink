<?php

/**
 * @file
 * Provides helper functions for custom forms.
 *
 * @link http://www.sharelink.com.au
 * @package Share Link
 */

/**
 * Builds the Share Link configuration form.
 *
 * @param object $form
 *   Form.
 * @param array $form_state
 *   Form state.
 *
 * @return array
 *   Renderable array of the configuration form.
 */
function sharelink_configuration($form, &$form_state) {
  $form = array('#tree' => TRUE, '#attributes' => array('class' => array('sharelink-configuration-form')));

  $sharelink_settings = sharelink_get_saved_settings();

  $level             = '';
  $stock_code        = '';
  $has_share         = FALSE;
  $has_graph         = FALSE;
  $has_announcements = FALSE;
  $json              = sharelink_get_json_from_server();

  $html = '<p>' . t('If you move servers or change domain names please !contact_us to organise an updated license key.', array('!contact_us' => l(t('contact us'), 'http://www.sharelink.com.au'))) . '</p>';

  if ($json === FALSE) {
    $html .= t('<div>There was an error checking the license. Please try again later.</div>');
  }
  else {

    $stock_code = $json->symbol;

    if (isset($json->has_share)) {
      $has_share = TRUE;
    }

    if (isset($json->has_graph)) {
      $has_graph = TRUE;
    }

    if (isset($json->has_announcements)) {
      $has_announcements = TRUE;
    }

    if (isset($json->has_announcements)) {
      $level = "gold";
    }
    elseif (isset($json->has_graph) && isset($json->has_share)) {
      $level = "silver";
    }
    elseif (isset($json->has_share)) {
      $level = "bronze";
    }

    $html .= t('<p>Sharelink license level <strong>!level</strong><br />', array('!level' => $level));
    $html .= t('Stock Code <strong>!stock_code</strong></p>', array('!stock_code' => $stock_code));
  }

  $form['sharelink_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Sharelink Options'),
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
  );

  $form['sharelink_settings']['information'] = array(
    '#markup' => t('<p>This page helps you set up the way Share Link will present ASX Announcements on your website.</p>
                   <p>Modify the options below to best suit your design, then add CSS styles to your template to customize further.</p>'),
  );

  $form['sharelink_settings']['level'] = array(
    '#type' => 'hidden',
    '#value' => $level,
  );

  $form['sharelink_settings']['has_share'] = array(
    '#type' => 'hidden',
    '#value' => $has_share,
  );

  $form['sharelink_settings']['has_graph'] = array(
    '#type' => 'hidden',
    '#value' => $has_graph,
  );

  $form['sharelink_settings']['has_announcements'] = array(
    '#type' => 'hidden',
    '#value' => $has_announcements,
  );

  $form['sharelink_settings']['stock_code'] = array(
    '#type'          => 'hidden',
    '#title'         => t('Stock Code'),
    '#value' => isset($stock_code) ? $stock_code : '',
    '#required'      => TRUE,
  );

  $form['sharelink_settings']['license_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('License Key'),
    '#description'   => t('Enter your license key you were given'),
    '#default_value' => isset($sharelink_settings['license_key']) ? $sharelink_settings['license_key'] : '',
    '#required'      => TRUE,
  );

  $form['sharelink_settings']['license_information'] = array(
    '#markup' => $html,
  );

  // Announcement option.
  if ($has_announcements) {

    $form['sharelink_settings']['display_type'] = array(
      '#type'          => 'select',
      '#title'         => t('Display Type'),
      '#description'   => t('Should the ASX announcements be display in a table or unordered list?'),
      '#options'       => array(
        'table' => t('Table Structure'),
        'list'  => t('Unordered List'),
      ),
      '#default_value' => isset($sharelink_settings['display_type']) ? $sharelink_settings['display_type'] : 'table',
    );

    $form['sharelink_settings']['display_announcements_by_month'] = array(
      '#type'          => 'select',
      '#title'         => t('Display Annoucements By Month'),
      '#description'   => t('Group / filter announcements by month'),
      '#options'       => array(
        '0' => t('No'),
        '1' => t('Yes'),
      ),
      '#default_value' => isset($sharelink_settings['display_announcements_by_month']) ? $sharelink_settings['display_announcements_by_month'] : '0',
      '#states' => array(
        'visible' => array(
          ':input[name="sharelink_settings[display_type]"]' => array('value' => 'list'),
        ),
      ),
    );

    $form['sharelink_settings']['display_announcements_by_month_header'] = array(
      '#type'          => 'select',
      '#title'         => t('Display Annoucements By Month Header'),
      '#options'       => array(
        'F Y' => date('F Y'),
        'F y' => date('F y'),
        'M Y' => date('M Y'),
        'F' => date('F'),
        'M' => date('M'),
      ),
      '#default_value' => isset($sharelink_settings['display_announcements_by_month_header']) ? $sharelink_settings['display_announcements_by_month_header'] : 'F Y',
      '#states' => array(
        'visible' => array(
          ':input[name="sharelink_settings[display_announcements_by_month]"]' => array('value' => '1'),
          ':input[name="sharelink_settings[display_type]"]' => array('value' => 'list'),
        ),
      ),
    );

    $form['sharelink_settings']['date_format'] = array(
      '#type'   => 'select',
      '#title'   => t('Date Format'),
      '#options' => array(
        'd/m/Y'     => date('d/m/Y'),
        'd/m/y'     => date('d/m/y'),
        'd M Y'     => date('d M Y'),
        'd F Y'     => date('d F Y'),
        'jS F Y'    => date('jS F Y'),
        'jS M Y'    => date('jS M Y'),
        'l, jS F Y' => date('l, jS F Y'),
      ),
      '#default_value' => isset($sharelink_settings['date_format']) ? $sharelink_settings['date_format'] : 'd/m/Y',
    );

    $form['sharelink_settings']['display_announcements_by_year'] = array(
      '#type'          => 'select',
      '#title'         => t('Display Annoucements By Year'),
      '#description'   => t('Group / filter announcements by year'),
      '#options'       => array(
        '0' => t('No'),
        '1' => t('Yes'),
      ),
      '#default_value' => isset($sharelink_settings['display_announcements_by_year']) ? $sharelink_settings['display_announcements_by_year'] : '0',
    );

    $form['sharelink_settings']['announcements_pagination'] = array(
      '#type'          => 'select',
      '#title'         => t('Include Pagination'),
      '#description'   => t('Seperate announcements into multiple pages (works by year, by month and number of announcements)'),
      '#options'       => array(
        '0' => t('No'),
        '1' => t('Yes'),
      ),
      '#default_value' => isset($sharelink_settings['announcements_pagination']) ? $sharelink_settings['announcements_pagination'] : '0',
    );

    $form['sharelink_settings']['announcements_per_page'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Announcements Per Page'),
      '#description'   => t('The number of announcements to display per page'),
      '#default_value' => isset($sharelink_settings['announcements_per_page']) ? $sharelink_settings['announcements_per_page'] : '50',
      '#required'      => TRUE,
      '#states' => array(
        'visible' => array(
          ':input[name="sharelink_settings[announcements_pagination]"]' => array('value' => '1'),
        ),
      ),
    );

    $form['sharelink_settings']['widget_date_format'] = array(
      '#type'   => 'select',
      '#title'   => t('Widget Date Format'),
      '#options' => array(
        'd/m/Y'     => date('d/m/Y'),
        'd/m/y'     => date('d/m/y'),
        'd M Y'     => date('d M Y'),
        'd F Y'     => date('d F Y'),
        'jS F Y'    => date('jS F Y'),
        'jS M Y'    => date('jS M Y'),
        'l, jS F Y' => date('l, jS F Y'),
      ),
      '#default_value' => isset($sharelink_settings['widget_date_format']) ? $sharelink_settings['widget_date_format'] : 'd/m/Y',
    );

    $form['sharelink_settings']['announcements_widget_limit'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Announcements Per Widget'),
      '#description'   => t('The number of announcements to display on the widget'),
      '#default_value' => isset($sharelink_settings['announcements_widget_limit']) ? $sharelink_settings['announcements_widget_limit'] : '5',
      '#required'      => TRUE,
    );

  }

  if ($has_graph) {
    $form['sharelink_settings']['graph_range'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Graph range for Version 3'),
      '#description'   => t('How many days back you wish start the graph in version 3'),
      '#default_value' => isset($sharelink_settings['graph_range']) ? $sharelink_settings['graph_range'] : '',
    );
  }

  return system_settings_form($form);
}

/**
 * Custom validation function for the Share Link configuration form.
 *
 * @param object $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function sharelink_configuration_validate($form, &$form_state) {
  // $values = isset($form_state['values']) ? $form_state['values'] : array();
}

/**
 * Custom form that allows user to filter announcements by Year.
 *
 * @param object $form
 *   Form.
 * @param array $form_state
 *   Form atate.
 * @param int $year
 *   The selected year.
 *
 * @return array
 *   Renderable array of the filter form
 */
function sharelink_announcements_filter_form($form, &$form_state, $year = FALSE) {
  $html               = '';
  $options            = array();
  $values             = isset($form_state['values']) ? $form_state['values'] : array();
  $sharelink_settings = sharelink_get_saved_settings();

  // Set the default year to the current if it doesn't already exist.
  if (!isset($values['asx_announcement_year']) && $year === FALSE) {
    $values['asx_announcement_year'] = date('Y');
  }
  elseif (isset($values['asx_announcement_year'])) {
    // Do nothing use this.
  }
  elseif ($year !== FALSE) {
    $values['asx_announcement_year'] = $year;
  }

  // Build up the options.
  for ($i = sharelink_get_first_announcement_date(); $i <= date('Y'); $i++) {
    $options[$i] = $i;
  }

  $form = array('#attributes' => array('class' => array('sl-asx-announcements-year-selct-form')));

  $form['asx_announcement_year'] = array(
    '#type'          => 'select',
    '#title'         => t('Year'),
    '#default_value' => isset($values['asx_announcement_year']) ? $values['asx_announcement_year'] : date('Y'),
    '#options'       => $options,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit'),
  );

  if ($sharelink_settings['display_type'] == 'table') {
    $html .= theme('sharelink_render_asx_announcements_table', array('year' => $values['asx_announcement_year']));
  }
  else {
    $html .= theme('sharelink_render_asx_announcements_unordered_list', array('year' => $values['asx_announcement_year']));
  }

  $form['results'] = array(
    '#markup' => $html,
    '#weight' => 300,
  );

  return $form;
}

/**
 * Custom filter form validator.
 *
 * @param object $form
 *   Form.
 * @param array $form_state
 *   Form atate.
 */
function sharelink_announcements_filter_form_validate($form, &$form_state) {
  // $values = isset($form_state['values']) ? $form_state['values'] : array();
}

/**
 * Custom filter form submit handler.
 *
 * @param object $form
 *   Form.
 * @param array $form_state
 *   Form atate.
 */
function sharelink_announcements_filter_form_submit($form, &$form_state) {
  $sharelink_settings = sharelink_get_saved_settings();
  $values = isset($form_state['values']) ? $form_state['values'] : array();

  $fields_to_store = array(
    'asx_announcement_year',
  );

  // Store values.
  foreach ($fields_to_store as $field) {
    $form_state['storage'][$field] = $values[$field];
  }

  // Rebuild the form.
  $form_state['rebuild'] = TRUE;

  if (isset($sharelink_settings['announcements_pagination']) && $sharelink_settings['announcements_pagination'] == 1) {
    drupal_goto(
      current_path(),
      array('query' => array('year' => $values['asx_announcement_year'])),
      '301'
    );
  }
}
