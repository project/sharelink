<?php

/**
 * @file
 * Stores all the drupal hook_override functions.
 *
 * @link http://www.sharelink.com.au
 * @package Share Link
 */

module_load_include('inc', 'sharelink', 'sharelink');
module_load_include('inc', 'sharelink', 'sharelink.utils');

define("SHARELINK_SOURCE", "//data.sharelink.com.au/");

/**
 * Implements hook_menu().
 */
function sharelink_menu() {
  $items = array();

  $items['admin/config/sharelink'] = array(
    'title'            => 'Share Link',
    'position'         => 'right',
    'page callback'    => 'system_admin_menu_block_page',
    'access arguments' => array('administer sharelink configuration'),
    'file'             => 'system.admin.inc',
    'file path'        => drupal_get_path('module', 'system'),
  );

  $items['admin/config/sharelink/sharelink'] = array(
    'title'            => 'Sharelink Configuration',
    'description'      => 'Manage Share Link related configuration',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('sharelink_configuration'),
    'access arguments' => array('administer sharelink configuration'),
    'file'             => 'sharelink.forms.inc',
  );

  // Without the param.
  $items['sharelink_download'] = array(
    'title'            => 'Download Announcements',
    'page callback'    => 'sharelink_download_announcements',
    'access arguments' => array('access content'),
    'type'             => MENU_CALLBACK,
    'file'             => 'sharelink.inc',
  );

  $items['sharelink_delete/%'] = array(
    'title'            => 'Delete Announcements',
    'page callback'    => 'sharelink_delete_announcements',
    'page arguments'   => array(1),
    'access arguments' => array('access content'),
    'type'             => MENU_CALLBACK,
    'file'             => 'sharelink.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function sharelink_permission() {
  return array(
    'administer sharelink configuration' => array(
      'title'       => t('Administer Sharelnik Configuration'),
      'description' => t('This permission permits a user to use the Sharelink Configuration form'),
    ),
  );
}

/**
 * Implements hook_token_info().
 */
function sharelink_token_info() {
  $info = array();

  $info['types']['sharelink'] = array(
    'name'        => t('Sharelink'),
    'description' => t('Tokens made availalbe by Sharelink for ASX Announcements, Share Price and Graphs'),
  );

  $info['tokens']['sharelink']['asx-announcements'] = array(
    'name'        => t('ASX Announcements'),
    'description' => t('Places the ASX announcements on the page as selected in settings'),
  );

  $info['tokens']['sharelink']['share-price-box'] = array(
    'name'        => t('Current ASX Share Price Box'),
    'description' => t('Display the current ASX Share price on the website'),
  );

  $info['tokens']['sharelink']['share-price-strip'] = array(
    'name'        => t('Current ASX Share Price Strip'),
    'description' => t('Display the current ASX Share price on the website'),
  );

  $info['tokens']['sharelink']['share-price-table'] = array(
    'name'        => t('Current ASX Share Price Table'),
    'description' => t('Display the current ASX Share price on the website'),
  );

  $info['tokens']['sharelink']['asx-graph-v1'] = array(
    'title'       => t('Current ASX Graph'),
    'description' => t('Display the current ASX Graph on the website'),
  );

  $info['tokens']['sharelink']['asx-graph-v3'] = array(
    'title'       => t('Current ASX Graph V3'),
    'description' => t('Display the current ASX Graph V3 on the website'),
  );

  $info['tokens']['sharelink']['latest-announcements-widget'] = array(
    'title'       => t('Latest Announcements Widget'),
    'description' => t('Displays the latest configured number of announcements'),
  );

  return $info;
}

/**
 * Implements hook_token().
 */
function sharelink_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'sharelink') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'asx-announcements':
          $replacements[$original] = theme('sharelink_render_asx_announcements');
          break;

        case 'share-price-box':
        case 'share-price-strip':
        case 'share-price-table':
          $replacements[$original] = theme('sharelink_render_asx_price', array('token' => $name));
          break;

        case 'asx-graph-v1':
        case 'asx-graph-v3':
          $replacements[$original] = theme('sharelink_render_graph', array('token' => $name));
          break;

        case 'latest-announcements-widget':
          $replacements[$original] = theme('sharelink_render_latest_announcements');
          break;
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_theme().
 */
function sharelink_theme($existing, $type, $theme, $path) {
  return array(
    'sharelink_help' => array(
      'template' => 'sharelink-help',
      'path'     => $path . '/templates',
    ),
    'sharelink_render_latest_announcements' => array(
      'file' => 'sharelink.theme.inc',
    ),
    'sharelink_render_asx_announcements' => array(
      'file' => 'sharelink.theme.inc',
    ),
    'sharelink_render_asx_price' => array(
      'variables' => array('token' => NULL),
      'file' => 'sharelink.theme.inc',
    ),
    'sharelink_render_graph' => array(
      'variables' => array('token' => NULL),
      'file' => 'sharelink.theme.inc',
    ),
    'sharelink_render_asx_announcements_table' => array(
      'variables' => array('year' => NULL),
      'file' => 'sharelink.theme.inc',
    ),
    'sharelink_render_asx_announcements_unordered_list' => array(
      'variables' => array('year' => NULL),
      'file' => 'sharelink.theme.inc',
    ),
  );
}

/**
 * Implements hook_help().
 */
function sharelink_help($path, $arg) {
  if ($path == 'admin/help#sharelink') {
    if (current_path() != 'admin/help/sharelink') {
      // Because system_modules() executes hook_help() for each module to 'test'
      // If they will return anything, but not actually display it, we want to
      // return a TRUE value if this is not actually the help page.
      return TRUE;
    }

    return theme('sharelink_help');
  }
}

/**
 * Implements hook_entity_info().
 */
function sharelink_entity_info() {
  return array(
    'sharelink_announcement' => array(
      'label'                  => t('Sharelink ASX Announcement'),
      'entity class'           => 'entity',
      'controller class'       => 'EntityAPIController',
      'base table'             => 'sharelink_announcement',
      'fieldable'              => FALSE,
      'entity keys'            => array(
        'id'                   => 'id',
        'label'                => 'title',
      ),
      'bundles'                => array(),
      'view modes'             => array(
        'full'                 => array(
          'label'              => t('Full Content'),
          'custom settings'    => FALSE,
        ),
      ),
      'label callback'         => 'sharelink_label_callback',
      'uri callback'           => 'entity_class_uri',
      'load hook'              => 'sharelink',
      'access callback'        => 'sharelink_entity_access',
      'module'                 => 'sharelink',
      'views controller class' => 'EntityDefaultViewsController',
    ),
  );
}

/**
 * Implements callback_entity_info_label()
 */
function sharelink_label_callback($announcement, $type) {
  return empty($announcement->title) ? 'Sharelink ASX Announcement' : $announcement->title;
}

/**
 * Sharelink entity access callback.
 */
function sharelink_entity_access($op, $entity_type, $entity = NULL, $account = NULL) {
  return TRUE;
}

/**
 * Function Sharelink Load.
 *
 * @param int $id
 *   Id of an announcement.
 * @param bool $reset
 *   Boolean TRUE or FALSE.
 *
 * @return array
 *   Array of announcments
 */
function sharelink_load($id, $reset = FALSE) {
  $asx_announcements = sharelink_load_multiple(array($id), array(), $reset);
  return reset($asx_announcements);
}

/**
 * Load multiple tasks based on certain conditions.
 */
function sharelink_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('sharelink_announcement', $ids, $conditions, $reset);
}

/**
 * Save task.
 */
function sharelink_save($asx_announcement) {
  entity_save('sharelink_announcement', $asx_announcement);
}

/**
 * Delete single task.
 */
function sharelink_delete($asx_announcement) {
  entity_delete('sharelink_announcement', entity_id('sharelink_announcement', $asx_announcement));
}

/**
 * Delete multiple tasks.
 */
function sharelink_delete_multiple($asx_announcement_ids) {
  // First we need to delete the files.
  $asx_announcements = sharelink_load_multiple($asx_announcement_ids);

  if (is_array($asx_announcements) && !empty($asx_announcements)) {
    foreach ($asx_announcements as $asx_announcement) {
      // Check the fid (file id) exists.
      if (isset($asx_announcement->fid) && !empty($asx_announcement->fid)) {
        // First we need to load the file.
        $file = file_load($asx_announcement->fid);

        // FILE EXISTS.
        if ($file) {
          file_delete($file);
        }
      }
    }
  }

  entity_delete_multiple('sharelink_announcement', $asx_announcement_ids);
}
