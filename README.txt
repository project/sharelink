-- SUMMARY --

Automatically manage the addition of ASX share price, commodity prices and ASX
Announcements to your website via the Share Link Drupal module.

Share Link is a subscription service that includes the ability to display share
prices, commodity prices, graphs and automatically upload ASX Announcements to
your website.

This plugin allows customisation of the presentation of ASX Announcements
received via Share Link. This includes display style, pagination, groupings by
year, month and more.

Note: requires a subscription to Share Link available from
http://sharelink.com.au

Dependencies
------------
 * entity
 * token
 * token_filter

-- INSTALLATION --

1) Go to your modules page (admin/modules) and select the sharelink module.
   Enable and save the page.

2) Once installed there are a couple of configurations that need to be done for
   the tokens to be processed by your Drupal site.

3) If you have Token and Token Filter enabled, here's how to add tokens to your
   fields:
  a) Go to Configuration > Text formats (admin/config/content/formats)
  b) Click Configure next to the text format you want to use. Be careful with
     this - do not choose a text format that your regular site visitors will be
     able to use.
  c) Check the "Replace tokens" box, as in the image below.
  d) Click Save Configuration.
  e) The second step we advise is to ensure the order of the replace tokens
     comes after "Convert line breaks into HTML"


  -- CONTACT --

  Current maintainers:
  * Nathan Rzepecki - https://www.drupal.org/u/lionslair
