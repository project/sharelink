<?php

/**
 * @file
 * Handles the install and uninstall of the module.
 *
 * @link http://www.sharelink.com.au
 * @package Share Link
 */

/**
 * Implements hook_schema().
 */
function sharelink_schema() {
  $schema['sharelink_announcement'] = array(
    'description' => 'Stores information about ASX Announcements',
    'fields' => array(
      'id' => array(
        'type'        => 'serial',
        'not null'    => TRUE,
        'description' => 'Primary Key: Unique ASX Announcement',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the node was created.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'date' => array(
        'description' => 'Date of the Announcement',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
      ),
      'title' => array(
        'description' => 'Title of the Announcement',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => FALSE,
      ),
      'fid' => array(
        'description' => 'The saved FID',
        'type'        => 'int',
        'length'      => 20,
        'not null'    => FALSE,
        'default'     => 0
      ),
      'url' => array(
        'description' => 'URL of the Announcement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE
      ),
    ),
    'primary key' => array('id'),
  );

  return $schema;
}

/**
 * Add the url field to the sharelink table.
 */
function sharelink_update_7100() {
  $spec = array(
    'description' => 'URL of the Announcement',
    'type' => 'varchar',
    'length' => 255,
    'not null' => FALSE
  );
  db_add_field('sharelink_announcement', 'url', $spec);
}

/**
 * Implements hook_install().
 */
function sharelink_install() {
  // Create the direcotry that will store the files.
  $sharelink_path = 'public://sharelink';
  if (file_prepare_directory($sharelink_path, FILE_CREATE_DIRECTORY)) {
    watchdog('sharelink', 'The sharelink directory was created', array(), WATCHDOG_INFO);
  }
  else {
    watchdog('sharelink', 'The sharelink directory was NOT created', array(), WATCHDOG_ERROR);
  }

}

/**
 * Implements hook_uninstall().
 */
function sharelink_uninstall() {

  // Delete all announcements.
  $results = db_select('sharelink_announcement', 'sla')->fields('sla', array('fid'))->execute()->fetchAll();

  if (is_array($results) &&!empty($results)) {
    foreach ($results as $result) {
      $file = file_load($result->fid);

      // FILE EXISTS.
      if ($file) {
        file_delete($file);
      }
    }
  }

  // Delete the stored files.
  drupal_rmdir('public://sharelink');

  // Delete the table.
  drupal_uninstall_schema('sharelink_announcement');

  // Delete the variables.
  variable_del('sharelink_settings');
}
