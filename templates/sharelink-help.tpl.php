<?php

/**
 * @file
 * This page contains all the help information.
 */
?>
<dl>
<dt><?php print t('Share Link for Drupal Help'); ?></dt>
<dd>

  <h3>Contents</h3>

  <ul>
    <li><a href="#dependencies">Dependencies</a></li>
    <li><a href="#install">Install</a></li>
    <li><a href="#configuration">Configuration</a></li>
    <li><a href="#tokens">How to use</a></li>
    <li><a href="#uninstall">Uninstall</a></li>
  </ul>


  <h3>Overview</h3>
  <p>You can place the share, commodity and currency using a short tag directly within your content. The content can then be styled as per the instructions provided to you at purchase time or <?php print l(t('download here'), 'http://sharelink.com.au/download/Share-Link-Install-Instructions.pdf', array('attributes' => array('target' => '_blank'))); ?></p>

  <a name="dependencies"></a>
  <h3>Dependencies</h3>
  <p>Sharelink module requires the following dependencies will be checked on install.</p>
  <ul>
    <li><?php print l(t('entity'), 'https://www.drupal.org/project/entity', array('attributes' => array('target' => '_blank'))); ?></li>
    <li><?php print l(t('token'), 'https://www.drupal.org/project/token', array('attributes' => array('target' => '_blank'))); ?></li>
    <li><?php print l(t('token_filter'), 'https://www.drupal.org/project/token_filter', array('attributes' => array('target' => '_blank'))); ?></li>
  </ul>

  <a name="install"></a>
  <h3>Install</h3>
  <p>Go to your modules page (admin/modules) and select the sharelink module. Enable and save the page.</p>
  <img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sharelink_install.png" alt="sharelink install" />

  <p>One installed there are a couple of configurations that need to be done for the tokens to be processed by your drupal site.</p>

  <p>If you have Token and Token Filter enabled, here's how to add tokens to your fields:</p>

 <ul>
    <li>Go to Configuration > Text formats (admin/config/content/formats)</li>
    <li>Click Configure next to the text format you want to use. Be careful with this - do not choose a text format that your regular site visitors will be able to use.</li>
    <li>Check the "Replace tokens" box, as in the image below.</li>
    <li>Click Save Configuration.</li>
 </ul>

 <p>The second step we advise is to ensure the order of the replace tokens comes after "Convert line breaks into HTML "</p>
 <img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/text_format_enable_replace_tokens.png" alt="sharelink text formats" />

  <a name="configuration"></a>
  <h3>Configuration</h3>
  <p>Elements of the Share Link module can be altered on the <?php print l(t('Configuration Page'), 'admin/config/sharelink/sharelink'); ?></p>

  <a name="tokens"></a>
  <h3>Tokens</h3>
  <p>To include Share Link data anywhere in your website pages you can use the following tokens.</p>


  <table width="100%">
    <tr>
      <td><strong>Token</strong></td>
      <td><strong>Description</strong></td>
      <td><strong>Example</strong></td>
    </tr>
    <tr>
      <td>[sharelink:share-price-box]</td>
      <td>Displays the stock code, price and movement in the standard Share Link box/vertical style</td>
      <td><img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-box-example.png" alt="share link price box" /></td>

    </tr>
    <tr>
      <td>[sharelink:share-price-strip]</td>
      <td>Displays the stock code, price and movement in a horizontal or strip format</td>
      <td><img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-strip-example.png" alt="share link price strip" /></td>
    </tr>
    <tr>
      <td>[sharelink:share-price-table]</td>
      <td>Displays a complete list of all stock prices, commodities and exchange rates in a table</td>
      <td><img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-multi-example.png" alt="share link price table" /></td>
    </tr>
    <tr>
      <td>[sharelink:asx-graph-v1]</td>
      <td>Displays a graph of the share price (default) or of the code selected if parameter is present. <br />
      </td>
      <td><img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-graph-example.png" alt="share link graph v1" /></td>
    </tr>
    <tr>
      <td>[sharelink:asx-graph-v3]</td>
      <td>Displays a graph of the share price (default) or of the code selected if parameter is present. Uses the version 3 renderer. <br />
          You may customise the range of the graph on the <?php print l(t('configuration page'), 'admin/config/sharelink/sharelink'); ?>.
      </td>
      <td><img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-graph-3-example.png" alt="share link graph v3" /></td>
    </tr>

    <tr>
      <td>[sharelink:asx-announcements]</td>
      <td>Displays a table or an unordered list of the latest ASX Announcements<br />
          Options to modify the display typem, date format, pagenation and number of announcements per page can be found on the <?php print l(t('configuration page'), 'admin/config/sharelink/sharelink'); ?>.
      </td>
      <td>
      <img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-announcements-table.png" alt="share link announcements table" />
      <img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-announcements-ullist.png" alt="share link announcements ul list" />
      </td>
    </tr>

    <tr>
      <td>[sharelink:latest-announcements-widget]</td>
      <td>Displays an list of the latest X number of announcements<br />
          Options to modify the number of announcements per page can be found on the <?php print l(t('configuration page'), 'admin/config/sharelink/sharelink'); ?>.
      </td>
      <td><img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sl-announcements-widgets.png" alt="share link announcements widget" /></td>
    </tr>

    </table>

  <h3>Blocks</h3>
  <p>Because Share Link uses tokens you can create custom blocks and just insert the the token you wish to use within the block. You can then assign the block to the region you wish to use it.</p>

  <a name="uninstall"></a>
  <h3>Uninstall</h3>
  <p>Go to your modules page (admin/modules) and un-select the sharelink module. save the page.</p>
  <img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sharelink_uninstall.png" alt="sharelink uninstall" />

  <p>Then go to the uninstall tab and continue to uninstall. This will remove all components of sharelink on your site.</p>
  <img src="<?php print base_path() . drupal_get_path('module', 'sharelink'); ?>/images/sharelink_uninstall_remove.png" alt="sharelink uninstall remove" />
  </dd>
</dl>
