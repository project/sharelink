<?php

/**
 * @file
 * Provides helper functions for the module.
 *
 * @link http://www.sharelink.com.au
 * @package Share Link
 */

/**
 * Get the saved settings.
 *
 * @return array
 *   Returns the stored variables.
 */
function sharelink_get_saved_settings() {
  return variable_get('sharelink_settings', array());
}

/**
 * Gets the license key from the stored values.
 *
 * @return string
 *   Share Link license key.
 */
function sharelink_get_license() {
  $license            = '';
  $sharelink_settings = sharelink_get_saved_settings();

  if (!empty($sharelink_settings['license_key'])) {
    $license = $sharelink_settings['license_key'];
  }

  return $license;
}

/**
 * Gets the graph range from the stored settings.
 *
 * @return string
 *   Graph range.
 */
function sharelink_get_graph_range() {
  $range = '';
  $sharelink_settings = sharelink_get_saved_settings();

  if (isset($sharelink_settings['graph_range']) && !empty($sharelink_settings['graph_range'])) {
    $range = $sharelink_settings['graph_range'];
  }

  return $range;
}

/**
 * Gets the stock code from the stored values.
 *
 * @return string
 *   ASX stock code.
 */
function sharelink_get_stock() {
  $stock = '';
  $sharelink_settings = sharelink_get_saved_settings();

  if (isset($sharelink_settings['stock_code']) && !empty($sharelink_settings['stock_code'])) {
    $stock = $sharelink_settings['stock_code'];
  }

  return $stock;
}

/**
 * Builds the share link url.
 *
 * @return string
 *   Share Link url.
 */
function sharelink_generate_url() {
  return 'http:' . SHARELINK_SOURCE . sharelink_get_license() . "/announcements";
}

/**
 * Retrieves the data from Share Link servers.
 *
 * @return bool
 *   Returns json array or false.
 */
function sharelink_feed_from_server() {
  $url       = sharelink_generate_url();
  $json      = sharelink_fetch_form_remote($url);
  $json_feed = json_decode($json);
  $return    = array();

  foreach ($json_feed as $json_item) {
    //No longer relying on friendly file name as we are linking to data source rather than storing file
    //$json_item->url = sharelink_create_url_friendly_filename($json_item->title, $json_item->date, $json_item->link);
    $return[] = $json_item;
  }

  if ($return !== FALSE) {
    return $return;
  }
  else {
    return FALSE;
  }
}

/**
 * Creates a friendly filename for the announcement.
 *
 * @param string $title
 *   Title.
 * @param string $date
 *   Date.
 * @param string $file
 *   Filename.
 *
 * @return string
 *   Friendly filename.
 */
function sharelink_create_url_friendly_filename($title, $date, $file) {
  $date = date("Ymd", strtotime($date));
  $string = str_replace(" ", "-", $title);
  $string = preg_replace("/[^-A-Za-z0-9]/", "", $string);
  $string = strtolower($string);

  $arr = explode("/", $file);
  $str = $arr[count($arr) - 1];
  $str = sharelink_shuffle_string($str);

  return $date . "-" . $string . "-" . $str . ".pdf";
}

/**
 * Creates a hash from the string.
 *
 * @param string $string
 *   String of text.
 *
 * @return string
 *   An MD5 hash of the past in string.
 */
function sharelink_shuffle_string($string) {
  return md5($string);
}

/**
 * Perform the CURL request to the Share Link servers.
 *
 * @param string $url
 *   The URL to fetch.
 *
 * @return array
 *   Response from the curl request.
 */
function sharelink_fetch_form_remote($url) {
  $response = drupal_http_request($url);

  // If an error occurred during processing, log the message and exit.
  if (isset($response->error)) {
    watchdog('sharelink', 'Attempt to get the feed from Share Link Server failed with cURL error: @error', array('@error' => $response['error']), WATCHDOG_ERROR);
    return FALSE;
  }

  return $response->data;
}

/**
 * Alters Schema to include new url column
 *
 * @return bool
 *   True or False.
 */

/**
 * Implements hook_schema_alter()
 */
function sharelink_schema_alter( &$schema ) {
  $schema['sharelink_announcement']['fields']['url'] = array(
    'description' => 'URL of the Announcement',
    'type' => 'varchar',
    'length' => 255,
    'not null' => 'TRUE'
  );
}

/**
 * Saves the announcement locally on disk.
 *
 * @param object $item
 *   The announcement ovbject.
 *
 * @return bool
 *   True or False.
 */
function sharelink_save_announcement_locally($item) {

  // Disabling file downloading to account for new data method
  //$file = system_retrieve_file($item->link, 'public://sharelink', TRUE, FILE_EXISTS_REPLACE);

  // Ensure date format is correct
  $dateobj = DateTime::createFromFormat('d/m/Y',$item[0]);
  $date = $dateobj->format('Y/m/d') . ' 00:00:00';

  $title = $item[2];
  $url = $item[4];

  $announcement = entity_create('sharelink_announcement', array(
    'created' => time(),
    'date'    => strtotime($date),
    'title'   => strip_tags($title),
    'url'     => 'http://www.asx.com.au'.$url,
    'fid'     => 0
  ));

  if (!sharelink_check_announcement_exists(strtotime($date), $title)) {
    if (entity_save('sharelink_announcement', $announcement)) {
      // Do nothing record saved.
      watchdog('sharelink', 'Announcement saved file !file', array('!file' => $title), WATCHDOG_NOTICE);
      return TRUE;
    }
    else {
      watchdog('sharelink', 'ASX Announcement not saved to database', array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }
  else {
    return FALSE;
  }

  // DISABLING OLD CODE BUT KEEPING FOR REFERENCE
  /*if ($file) {

    if ($announcement = sharelink_check_announcement_exists(strtotime($item->date), $item->title)) {
      $announcement->fid = $file->fid;
      entity_save('sharelink_announcement', $announcement);
      watchdog('sharelink', 'Announcement already existed, UPDATED file !file', array('!file' => $item->link), WATCHDOG_NOTICE);
    }
    else {
      $announcement = entity_create('sharelink_announcement', array(
        'created' => time(),
        'date'    => strtotime($item->date),
        'title'   => strip_tags($item->title),
        'fid'     => $file->fid,
      ));

      if (entity_save('sharelink_announcement', $announcement)) {
        // Do nothing record saved.
        watchdog('sharelink', 'Announcement saved file !file', array('!file' => $item->link), WATCHDOG_NOTICE);
        return TRUE;
      }
      else {
        watchdog('sharelink', 'ASX Announcement not saved to database', array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }

  }
  else {
    watchdog('sharelink', 'Failed to fetch file !file', array('!file' => $item->link), WATCHDOG_NOTICE);
    return FALSE;
  }*/
}
