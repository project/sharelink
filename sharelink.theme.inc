<?php

/**
 * @file
 * Provides theme functions for rendering output.
 *
 * @link http://www.sharelink.com.au
 * @package Share Link
 */

/**
 * Renders the ASX announcments.
 *
 * @return string
 *   HTML string of anouncements
 */
function theme_sharelink_render_asx_announcements() {
  module_load_include('inc', 'sharelink', 'sharelink.forms');
  $html = '';

  $sharelink_settings = sharelink_get_saved_settings();

  if (isset($sharelink_settings['display_announcements_by_year']) && $sharelink_settings['display_announcements_by_year'] == 1) {

    // Default value.
    $year = FALSE;

    $url_params = drupal_get_query_parameters();

    if (isset($url_params['year']) && !empty($url_params['year'])) {
      $year = $url_params['year'];
    }

    $form = drupal_get_form('sharelink_announcements_filter_form', $year);
    $html .= drupal_render($form);

  }
  else {
    if (isset($sharelink_settings['display_type']) && $sharelink_settings['display_type'] == 'table') {
      $html .= theme('sharelink_render_asx_announcements_table');
    }
    else {
      $html .= theme('sharelink_render_asx_announcements_unordered_list');
    }
  }

  return $html;
}

/**
 * Helper function to display the announcements for a given year.
 *
 * @param int $variables
 *   Contains an array with Year if passed.
 *
 * @return string
 *   Formatted html output.
 */
function theme_sharelink_render_asx_announcements_unordered_list($variables) {
  $year               = isset($variables['year']) ? $variables['year'] : FALSE;
  $html               = '';
  $sharelink_settings = sharelink_get_saved_settings();
  $announcements      = sharelink_get_announcements_from_database($year);
  $items              = array();
  $last_month_heading = '';

  if (!isset($sharelink_settings['date_format'])) {
    $sharelink_settings['date_format'] = 'd/m/Y';
  }

  if (is_array($announcements) && !empty($announcements)) {
    foreach ($announcements as $announcement) {

      // Only show the heading if it is turned on.
      if (isset($sharelink_settings['display_announcements_by_month']) && $sharelink_settings['display_announcements_by_month'] == 1) {
        $current_header = date($sharelink_settings['display_announcements_by_month_header'], $announcement->date);

        // Lets check if it is different from the last header.
        if ($current_header != $last_month_heading) {
          $items[] = array('data' => $current_header, 'class' => array('sl-month'));
          $last_month_heading = $current_header;
        }
      }

      $file = file_load($announcement->fid);
      if (!$file) {
        $url = $announcement->url;
      }
      else {
        $url = file_create_url($file->uri);
      }

      $item  = '<span class="sl-date">' . date($sharelink_settings['date_format'], $announcement->date) . '</span>&nbsp;';
      $item .= '<span class="sl-title">' . $announcement->title . '</span>&nbsp;';
      $item .= '<span>' . l(t('Download'), $url, array('attributes' => array('target' => '_blank', 'class' => array('sl-download-link')))) . '</span>';
      $items[] = array('data' => $item, 'class' => array('sl-announcement'));
    }
  }

  $list_vars = array(
    'items'      => $items,
    'title'      => t('ASX Announcements'),
    'type'       => 'ul',
    'attributes' => array(
      'class' => array('sharelink-asx-announcements'),
    ),
  );

  $html .= theme('item_list', $list_vars);

  // Paging.
  if (isset($sharelink_settings['announcements_pagination']) && $sharelink_settings['announcements_pagination'] == 1) {
    // Extra parameters.
    $parameters = array();

    // If not false add the year to the parameters.
    if ($year) {
      $parameters['year'] = $year;
    }

    $html .= theme('pager', array('parameters' => $parameters));
  }

  return $html;
}

/**
 * Helper function to display the announcements for a given year.
 *
 * @param int $variables
 *   Contains an array with Year if passed.
 *
 * @return string
 *   Rendered html output.
 */
function theme_sharelink_render_asx_announcements_table($variables) {
  $sharelink_settings = sharelink_get_saved_settings();
  $year               = isset($variables['year']) ? $variables['year'] : FALSE;

  $header = array(
    array('data' => t('Date'), 'class' => 'heading'),
    array('data' => t('Title'), 'class' => 'heading'),
    array('data' => '', 'class' => 'heading'),
  );

  $rows = array();

  $announcements = sharelink_get_announcements_from_database($year);

  if (is_array($announcements) && !empty($announcements)) {
    foreach ($announcements as $announcement) {

      $file = file_load($announcement->fid);
      if (!$file) {
        $url = $announcment->url;
      }
      else {
        $url = file_create_url($file->uri);
      }

      $row = array(
        date($sharelink_settings['date_format'], $announcement->date),
        $announcement->title,
        l(t('Download'), $url, array('attributes' => array('target' => '_blank', 'class' => array('sl-download-link')))),
      );

      $rows[] = $row;
    }
  }

  $variables = array(
    'header'     => $header,
    'rows'       => $rows,
    'attributes' => array('class' => array('sl-announcements')),
    'caption'    => '',
    'colgroups'  => array(),
    'sticky'     => FALSE,
    'empty'      => t('No Annoucements Found'),
  );

  $html  = theme('table', $variables);

  // Paging.
  if (isset($sharelink_settings['announcements_pagination']) && $sharelink_settings['announcements_pagination'] == 1) {
    // Extra parameters.
    $parameters = array();

    // If not false add the year to the parameters.
    if ($year) {
      $parameters['year'] = $year;
    }

    $html .= theme('pager', array('parameters' => $parameters));
  }

  return $html;
}

/**
 * Helper function that will render the ASX price.
 *
 * @param string $variables
 *   Token that will be sent to sharelink.
 *
 * @return string
 *   HTML string.
 */
function theme_sharelink_render_asx_price($variables) {
  $html = '';
  $token = isset($variables['token']) ? $variables['token'] : 'share-price-box';

  $type = substr($token, 12);

  $url = SHARELINK_SOURCE . sharelink_get_license() . "/html?type=" . $type . '&div=' . $token;

  $html .= '<script type="text/javascript" src="' . $url . '"></script><div id="' . $token . '"></div>';

  return $html;
}

/**
 * Helper function that will render the ASX Graph.
 *
 * @param string $variables
 *   Token that will be sent to sharelink.
 *
 * @return string
 *   HTML string.
 */
function theme_sharelink_render_graph($variables) {
  $html = '';
  $token = isset($variables['token']) ? $variables['token'] : 'asx-graph-v1';

  $version = substr($token, 11);
  $symbol = sharelink_get_stock();
  $range = sharelink_get_graph_range();

  $url = SHARELINK_SOURCE . sharelink_get_license() . "/graph?version=" . $version . '&symbol=' . $symbol;

  if ($version == 3 && !empty($range)) {
    $url .= '&start=' . $range . 'days';
  }

  $html .= '<script type="text/javascript" src="' . $url . '"></script><div id="sharelink-graph-' . $symbol . '"></div>';

  return $html;
}

/**
 * Function that builds the render for the output.
 *
 * @return string
 *   Formatted output for anouncements.
 */
function theme_sharelink_render_latest_announcements() {
  $html               = '';
  $sharelink_settings = sharelink_get_saved_settings();
  $limit              = 3;
  $items              = array();

  if (!isset($sharelink_settings['date_format'])) {
    $sharelink_settings['date_format'] = 'd/m/Y';
  }

  if (isset($sharelink_settings['announcements_widget_limit']) && !empty($sharelink_settings['announcements_widget_limit'])) {
    $limit = $sharelink_settings['announcements_widget_limit'];
  }

  $html .= '<div class="sl-announcements-widget">';

  $announcements = sharelink_get_announcements_from_database(FALSE, $limit);

  if (is_array($announcements) && !empty($announcements)) {
    foreach ($announcements as $announcement) {

      $file = file_load($announcement->fid);
      if (!$file) {
        $url = $announcement->url;
      }
      else {
        $url = file_create_url($file->uri);
      }

      $item  = '<span class="sl-date">' . date($sharelink_settings['date_format'], $announcement->date) . '</span>&nbsp;';
      $item .= '<span class="sl-title">' . l($announcement->title, $url, array('attributes' => array('target' => '_blank', 'class' => array('sl-download-link')))) . '</span>&nbsp;';

      $items[] = array('data' => $item, 'class' => array('sl-announcement'));
    }
  }

  $list_vars = array(
    'items'      => $items,
    'title'      => t('ASX Announcements'),
    'type'       => 'ul',
    'attributes' => array(
      'class' => array('sharelink-asx-announcements'),
    ),
  );

  $html .= theme('item_list', $list_vars);
  $html .= '</div>';

  return $html;
}
