<?php

/**
 * @file
 * Provides helper functions for Share Link module.
 *
 * @link http://www.sharelink.com.au
 * @package Share Link
 */

 /**
  * Delete Announcements for a given year.
  *
  * @param int $year
  *   Year 2016.
  */
function sharelink_delete_announcements($year = FALSE) {

  $tz_perth = new DateTimeZone(drupal_get_user_timezone());
  $start = new DateTime('now', $tz_perth);

  $start->setDate($year, 1, 1);
  $start->setTime(0, 0, 0);

  $end = clone $start;
  // Plus + 1 month.
  $end->add(new DateInterval('P12M'));

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'sharelink_announcement');

  if ($year) {
    $query->propertyCondition('date', $start->getTimestamp(), '>=')
      ->propertyCondition('date', $end->getTimestamp(), '<=');
  }

  $results = $query->execute();

  if (isset($results['sharelink_announcement']) && is_array($results['sharelink_announcement']) && !empty($results['sharelink_announcement'])) {
    sharelink_delete_multiple(array_keys($results['sharelink_announcement']));
  }

  echo "Announcements and Files Deleted.";
}

/**
 * Downloads the latest announcements from sharelink.
 */
function sharelink_download_announcements() {
  $feed = sharelink_feed_from_server();

  if (is_array($feed) && !empty($feed)) {
    //Check database to see if new column exists
    $schema = drupal_get_schema('sharelink_announcement');
    if (!db_field_exists('sharelink_announcement','url')) {
      $new_schema = array();
      sharelink_schema_alter($new_schema);
      foreach ($new_schema['sharelink_announcement']['fields'] as $field_name => $field_spec) {
        db_add_field('sharelink_announcement',$field_name,$field_spec);
      }
    }
    foreach ($feed as $item) {
      if (sharelink_save_announcement_locally($item)) {
        echo check_plain($item[2]) . " added<br />";
      }
      else {
        echo check_plain($item[2]) . " not added<br />";
      }
    }
  }

  echo "Download complete<br />";
}

/**
 * Verifys license with Share Link.
 *
 * @return bool
 *   returns true or false
 */
function sharelink_verify() {

  $json = sharelink_get_json_from_server();

  if ($json->active == 1) {
    return TRUE;
  }
  else {
    return FALSE;
  }

}

/**
 * Retrieves the JSON data from Share Link servers.
 *
 * @return array
 *   Returns JSON data else false
 */
function sharelink_get_json_from_server() {
  global $base_url;

  $license            = '';
  $sharelink_settings = sharelink_get_saved_settings();

  if (!empty($sharelink_settings['license_key'])) {
    $license = $sharelink_settings['license_key'];
  }

  $url = 'http:' . SHARELINK_SOURCE . $license . "/verify";
  $opts = array(
    'http' => array(
      'header' => array("Referer: $base_url\r\n"),
    ),
  );
  $context = stream_context_create($opts);
  $result = file_get_contents($url, FALSE, $context);

  if ($result === FALSE ||
      $result == 'Invalid license length' ||
      $result == 'All domains are invalid') {
    drupal_set_message(t('Sharelink returned an error: !result', array('!result' => $result)), 'error', FALSE);
    watchdog('sharelink', 'Sharelink returned an error: !result', array('!result' => $result), WATCHDOG_ERROR);
    return FALSE;
  }
  else {
    return json_decode($result);
  }
}

/**
 * Tests if the current announcement exists.
 *
 * @param string $date
 *   A date.
 * @param string $title
 *   Title of the announcement.
 *
 * @return bool
 *   True or False
 */
function sharelink_check_announcement_exists($date, $title = '') {
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'sharelink_announcement')
    ->propertyCondition('date', $date)
    ->propertyCondition('title', $title)
    ->range(0, 1)
    ->execute();

  if (isset($result['sharelink_announcement']) && !empty($result['sharelink_announcement'])) {
    $sharelink_announcement_ids = array_keys($result['sharelink_announcement']);
    $sharelink_announcement_id = reset($sharelink_announcement_ids);
    $sharelink_announcement = sharelink_load($sharelink_announcement_id);
    return $sharelink_announcement;
  }

  return FALSE;
}

/**
 * Gets the first stored ASX announcement year.
 *
 * @return int
 *   Year
 */
function sharelink_get_first_announcement_date() {
  $query = new EntityFieldQuery();
  $results = $query->entityCondition('entity_type', 'sharelink_announcement')
    ->propertyOrderBy('date', 'ASC')
    ->range(0, 1)
    ->execute();

  if (isset($results['sharelink_announcement']) &&
      is_array($results['sharelink_announcement']) &&
      !empty($results['sharelink_announcement'])) {

    $keys = array_keys($results['sharelink_announcement']);
    $first = reset($keys);

    $asx_announcement = sharelink_load($first);

    if ($asx_announcement) {
      return date('Y', $asx_announcement->date);
    }
  }

  return date('Y');
}

/**
 * Queries the database for given announcements.
 *
 * @param int $year
 *   Year.
 * @param int $limit
 *   Number to linit the anouncements to.
 *
 * @return arrray
 *   Array or anouncements
 */
function sharelink_get_announcements_from_database($year = FALSE, $limit = FALSE) {
  $rows               = array();
  $sharelink_settings = sharelink_get_saved_settings();

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'sharelink_announcement')
    ->propertyOrderBy('date', 'DESC');

  // Do we want to limit by year.
  if (isset($sharelink_settings['display_announcements_by_year']) && $sharelink_settings['display_announcements_by_year'] == 1) {

    if ($year) {
      // This should be ok do nothing.
    }
    else {
      $year = date('Y');
    }

    $tz_perth = new DateTimeZone(drupal_get_user_timezone());
    $start = new DateTime('now', $tz_perth);

    $start->setDate($year, 1, 1);
    $start->setTime(0, 0, 0);

    $end = clone $start;
    // Plus 1 month.
    $end->add(new DateInterval('P12M'));

    $query->propertyCondition('date', $start->getTimestamp(), '>=')
      ->propertyCondition('date', $end->getTimestamp(), '<=');

  }

  // If limit is set.
  if ($limit) {
    $query->range(0, $limit);
  }

  // Check if paging is on.
  // Had to add the third check because limit is over ridden when pager is
  // invoked.
  if (isset($sharelink_settings['announcements_pagination']) &&
      $sharelink_settings['announcements_pagination'] == 1 &&
      $limit === FALSE) {
    if (isset($sharelink_settings['announcements_per_page']) && !empty($sharelink_settings['announcements_per_page'])) {
      $query->pager($sharelink_settings['announcements_per_page']);
    }
  }

  $results = $query->execute();

  if (isset($results['sharelink_announcement']) &&
      is_array($results['sharelink_announcement']) &&
      !empty($results['sharelink_announcement'])) {

    $rows = sharelink_load_multiple(array_keys($results['sharelink_announcement']));
  }

  return $rows;
}
